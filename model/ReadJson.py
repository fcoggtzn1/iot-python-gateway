import json

with open('../assets/model/example.json') as json_file:
    data = json.load(json_file)
    print(data)

data["system"]["version"]=2.0
# Directly from dictionary
with open('../assets/model/new_example.json', 'w') as outfile:
    json.dump(data, outfile)